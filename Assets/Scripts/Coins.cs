﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Coins : MonoBehaviour
{
	public List<GameObject> column_of_coins = new List<GameObject> ();

	// Use this for initialization
	void Start ()
	{


	}

	void Drawing_list_of_coins (GameObject coin)
	{	
		RaycastHit2D hitUp = Physics2D.Raycast (
			new Vector2 (coin.transform.position.x, coin.transform.position.y + 51), Vector2.up);
		if (hitUp.collider) {
			if (!column_of_coins.Contains (hitUp.collider.gameObject)) {
				column_of_coins.Add (hitUp.collider.gameObject);
				Drawing_list_of_coins (hitUp.collider.gameObject);
			}
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		
		RaycastHit2D hitDown = Physics2D.Raycast (new Vector2 (transform.position.x, transform.position.y - 29), Vector2.zero);
		if (!hitDown.collider) {
			transform.position = Vector2.Lerp (transform.position, new Vector2 (transform.position.x, transform.position.y - 51), 0.2f);
		} 

		if (Mathf.Round (transform.position.y) == -179) {
			RaycastHit2D hitLeft = Physics2D.Raycast (new Vector2 (transform.position.x - 29, transform.position.y), Vector2.up);
			if (!hitLeft.collider) {
				column_of_coins.Add (gameObject);
				Drawing_list_of_coins (gameObject);
				for (int k = 0; k < column_of_coins.Count; k++) {
					column_of_coins [k].transform.position = Vector2.Lerp (column_of_coins [k].transform.position, 
					                                           new Vector2 (column_of_coins [k].transform.position.x - 51, 
					             column_of_coins [k].transform.position.y), 0.1f);
				}
			}
		}
		column_of_coins.Clear ();
	}
}
