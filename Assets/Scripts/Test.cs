﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using System;

public class Test : MonoBehaviour
{
	public GameObject green_coin;
	public GameObject red_coin;
	public GameObject yellow_coin;
	public List<GameObject> coins = new List<GameObject> ();
	public List<GameObject> bunch_of_coins = new List<GameObject> ();
	public List<GameObject> all_coins = new List<GameObject> ();
	public AudioClip sound;
	public bool trim = true;
	public bool isSound = true;
	public float t = 0;
	public bool b = false;
	public Text point;
	public Text p;
	public int pnt;
	public float tm = 0;
	public string highscores;
	public int k = 0;
	public System.Random rnd = new System.Random ();
	public bool soundOn = true;

	// Use this for initialization
	void Start ()
	{
		coins.Add (green_coin);
		coins.Add (red_coin);
		coins.Add (yellow_coin);
		for (float x = -227; x <= 232; x += 51) {
			for (float y = -179; y <= 178; y += 51) {
				all_coins.Add (Instantiate (coins [rnd.Next (0, coins.Count)], new Vector2 (x, y), Quaternion.identity) as GameObject);
			}
		}
	}

	public void LoadLevel ()
	{
		Application.LoadLevel ("Menu");
	}

	void Drawing_list_of_coins (GameObject touch)
	{
		b = true;
		RaycastHit2D hitUp = Physics2D.Raycast (
			new Vector2 (touch.transform.position.x, touch.transform.position.y + 51), Vector2.zero);
		RaycastHit2D hitDown = Physics2D.Raycast (
			new Vector2 (touch.transform.position.x, touch.transform.position.y - 51), Vector2.zero);
		RaycastHit2D hitRight = Physics2D.Raycast (
			new Vector2 (touch.transform.position.x + 51, touch.transform.position.y), Vector2.zero);
		RaycastHit2D hitLeft = Physics2D.Raycast (
			new Vector2 (touch.transform.position.x - 51, touch.transform.position.y), Vector2.zero);
		
		if (hitUp.collider && touch.tag == hitUp.collider.gameObject.tag) {
			if (!bunch_of_coins.Contains (hitUp.collider.gameObject)) {
				bunch_of_coins.Add (hitUp.collider.gameObject);
				Drawing_list_of_coins (hitUp.collider.gameObject);
			}
		}
		if (hitDown.collider && touch.tag == hitDown.collider.gameObject.tag) {
			if (!bunch_of_coins.Contains (hitDown.collider.gameObject)) {
				bunch_of_coins.Add (hitDown.collider.gameObject);
				Drawing_list_of_coins (hitDown.collider.gameObject);
			}
		}
		if (hitRight.collider && touch.tag == hitRight.collider.gameObject.tag) {
			if (!bunch_of_coins.Contains (hitRight.collider.gameObject)) {
				bunch_of_coins.Add (hitRight.collider.gameObject);
				Drawing_list_of_coins (hitRight.collider.gameObject);
			}
		}
	
		if (hitLeft.collider && touch.tag == hitLeft.collider.gameObject.tag) {
			if (!bunch_of_coins.Contains (hitLeft.collider.gameObject)) {
				bunch_of_coins.Add (hitLeft.collider.gameObject);
				Drawing_list_of_coins (hitLeft.collider.gameObject);
			}
		}
	}

	// Update is called once per frame
	void Update ()
	{
		/*if ((Input.deviceOrientation == DeviceOrientation.LandscapeLeft) && 
			(Screen.orientation != ScreenOrientation.LandscapeLeft)) {
			Screen.orientation = ScreenOrientation.LandscapeLeft;
		}
		
		if ((Input.deviceOrientation == DeviceOrientation.LandscapeRight) && 
			(Screen.orientation != ScreenOrientation.LandscapeRight)) {
			Screen.orientation = ScreenOrientation.LandscapeRight;
		}*/
		for (int i = 0; i < Input.touchCount; i++) {
			if (Input.GetTouch (i).phase == TouchPhase.Began && Input.touchCount == 1 && b == false) {
				RaycastHit2D hitInfo = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (Input.GetTouch (i).position), Vector2.zero);
				if (hitInfo.collider && hitInfo.collider.gameObject.tag != "floor" && hitInfo.collider.gameObject.tag != "wall") {
					Drawing_list_of_coins (hitInfo.collider.gameObject);

					foreach (GameObject coins in bunch_of_coins) {
						all_coins.Remove (coins);	
						coins.GetComponent <SpriteRenderer> ().sortingOrder = 1; 
						Destroy (coins.GetComponent <CircleCollider2D> ());
						Destroy (coins.GetComponent <Coins> ());
						coins.GetComponent <Move> ().p = true;
						if (isSound != false && soundOn == true) {
							AudioSource.PlayClipAtPoint (sound, transform.position);
							if (coins.GetComponent <SpriteRenderer> ().color == Color.white)
								point.color = Color.yellow;
							else
								point.color = coins.GetComponent <SpriteRenderer> ().color;
							isSound = false;
						}
					}
					isSound = true;
					if (bunch_of_coins.Count != 0) {
						point.text = "+" + "" + bunch_of_coins.Count.ToString ();
						pnt += bunch_of_coins.Count;
						p.text = pnt.ToString ();
						tm = Time.time;
					}	
				}
			}
		}
		if (bunch_of_coins.Count == 0) 
			b = false;

		if (all_coins.Count == 0 && trim == true) {
			pnt += 100;
			trim = false;
			t = Time.time + 3;
		}

		for (int i = 0; i < all_coins.Count; i++) {

			if (!all_coins [i]) {
				all_coins.RemoveAt (i);
				continue;
			}
			RaycastHit2D hitUp = Physics2D.Raycast (
				new Vector2 (all_coins [i].transform.position.x, all_coins [i].transform.position.y + 51), Vector2.up);
			RaycastHit2D hitDown = Physics2D.Raycast (
				new Vector2 (all_coins [i].transform.position.x, all_coins [i].transform.position.y - 51), -Vector2.up);
			RaycastHit2D hitRight = Physics2D.Raycast (
				new Vector2 (all_coins [i].transform.position.x + 51, all_coins [i].transform.position.y), Vector2.right);
			RaycastHit2D hitLeft = Physics2D.Raycast (
				new Vector2 (all_coins [i].transform.position.x - 51, all_coins [i].transform.position.y), -Vector2.right);
			
			if (hitUp.collider && all_coins [i].tag == hitUp.collider.gameObject.tag 
				|| hitDown.collider && all_coins [i].tag == hitDown.collider.gameObject.tag
				|| hitRight.collider && all_coins [i].tag == hitRight.collider.gameObject.tag 
				|| hitLeft.collider && all_coins [i].tag == hitLeft.collider.gameObject.tag) {
				break;
			} else {
				if (hitUp.collider && all_coins [i].tag != hitUp.collider.gameObject.tag 
					&& hitDown.collider && all_coins [i].tag != hitDown.collider.gameObject.tag
					&& hitRight.collider && all_coins [i].tag != hitRight.collider.gameObject.tag 
					&& hitLeft.collider && all_coins [i].tag != hitLeft.collider.gameObject.tag) {
					continue;
				}
				if (i == all_coins.Count - 1 && trim == true) {
					trim = false;
					t = Time.time + 3;
				}
			}			 
		}

		if (trim == false && Time.time > t) {
			highscores = PlayerPrefs.GetString ("0") + " " + pnt.ToString ();
			if (PlayerPrefs.HasKey ("1")) {
				PlayerPrefs.SetString ("1", PlayerPrefs.GetString ("1") + "\n" + highscores);
			} else
				PlayerPrefs.SetString ("1", highscores);
			Application.LoadLevel ("Menu");
		}
		if (Time.time - tm > 0.8f) 
			point.text = "";
	}
}
