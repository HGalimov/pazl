﻿using UnityEngine;
using System.Collections;


public class Move : MonoBehaviour
{
	public Transform startMarker;
	public Transform endMarker;
	public bool p = false;
	public bool isAdd = false;
	// Use this for initialization
	void Start ()
	{
		startMarker = transform;
		endMarker = GameObject.Find ("entry").transform;
	}

	void OnCollisionEnter2D (Collision2D coll)
	{
		if (coll.gameObject.tag == "entry") {
			GameObject.FindGameObjectWithTag ("MainCamera").GetComponent <Test> ().bunch_of_coins.Remove (gameObject);
			Destroy (gameObject);
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (p == true) {
			transform.position = Vector2.Lerp (startMarker.position, endMarker.position, 0.1f);
		}
		if (transform.position.x < GameObject.Find ("target").transform.position.x) {
			if (isAdd == false) {
				gameObject.AddComponent<CircleCollider2D> ();
				isAdd = true;
			}
		}
	}
}
