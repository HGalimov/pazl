﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;

public class Menu : MonoBehaviour
{
	public InputField inp;
	public Text txt;
	public Text rec;
	public float tm;
	public int k = 0;
	public string str = "";
	public Dictionary <string, int> records = new Dictionary <string, int> ();
	public bool b = false;
	public int c = 0;
	public int max;
	public Sprite soundOn;
	public Sprite soundOff;
	// Use this for initialization
	void Start ()
	{
	}

	public void LoadLevel1 ()
	{
		Application.LoadLevel ("UserName");
	}

	public void LoadLevel ()
	{
		Regex rgx = new Regex (@"^[A-Z][a-z]{2,8}$");
		if (inp.text.Length != 0 && rgx.IsMatch (inp.text)) {
			PlayerPrefs.SetString ("0", inp.text);
			Application.LoadLevel ("1");
		} else {
			txt.text = "Enter correct name";
			tm = Time.time;
		}
	}

	public void LoadLevel0 ()
	{
		Application.LoadLevel ("Menu");
	}

	public void LoadLevelR ()
	{
		//PlayerPrefs.DeleteAll ();

		Application.LoadLevel ("Records");
	}

	public void Clear ()
	{
		PlayerPrefs.DeleteAll ();
	}

	public void SoundOffOn ()
	{
		if (GameObject.FindGameObjectWithTag ("MainCamera").GetComponent <Test> ().soundOn == true) {
			GameObject.FindGameObjectWithTag ("MainCamera").GetComponent <Test> ().soundOn = false;	
			GameObject.FindGameObjectWithTag ("sound").GetComponent <Image> ().sprite = soundOn;
		} else {
			GameObject.FindGameObjectWithTag ("MainCamera").GetComponent <Test> ().soundOn = true;
			GameObject.FindGameObjectWithTag ("sound").GetComponent <Image> ().sprite = soundOff;
		}
	}

	// Update is called once per frame
	void Update ()
	{
		if ((Input.deviceOrientation == DeviceOrientation.LandscapeLeft) && 
			(Screen.orientation != ScreenOrientation.LandscapeLeft)) {
			Screen.orientation = ScreenOrientation.LandscapeLeft;
		}
		
		if ((Input.deviceOrientation == DeviceOrientation.LandscapeRight) && 
			(Screen.orientation != ScreenOrientation.LandscapeRight)) {
			Screen.orientation = ScreenOrientation.LandscapeRight;
		}
		if (txt) {
			if (Time.time - tm > 1f) 
				txt.text = "";
		}
		if (PlayerPrefs.HasKey ("1") && rec && b == false) {
			rec.text = PlayerPrefs.GetString ("1");
			string[] rec1 = PlayerPrefs.GetString ("1").Split ('\n');
			for (int i = 0; i < rec1.Length; i++) {
				string[] recs = rec1 [i].Split (' ');
				if (!records.ContainsKey (recs [0])) {
					records.Add (recs [0], Convert.ToInt32 (recs [1]));
				}
			}

			var sortedDict = (from entry in records orderby entry.Value descending select entry)
				.Take (5).ToDictionary (pair => pair.Key, pair => pair.Value);

			foreach (var elem in sortedDict) {	
				if (elem.Equals (sortedDict.ElementAt (0))) {
					str = elem.Key + " " + elem.Value.ToString ();
				} else 
					str += "\n" + elem.Key + " " + elem.Value.ToString ();
			}
			PlayerPrefs.SetString ("1", str);
			rec.text = str;
			b = true;
		}
	}
}
